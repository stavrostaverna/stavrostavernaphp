<?php
include 'includes/header.inc.php';
include 'includes/nav.inc.php';
?>
<div class="row equal">
	<div class="col-md-6">
		<div class="panel">
			<img class="img-responsive book-img" alt="Stavros Taverna Grekisk Restaurang" src="img/boka/stavros-in.jpg">
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading info-panel">
				<h3 class="text-center">
					<em><span style="padding-right: 10px;">Grekisk</span><span>Restaurang</span></em>
				</h3>
			</div>
			<div class="panel-body ">
				<p>
					Varmt välkomna till oss på <strong><em>Stavros Taverna</em></strong>, en liten Grekisk restaurang på Södermalm i Stockholm.
				</p>
				<p>Vi serverar genuina mezerätter &#8220;smårätter&#8221; som lagas enligt traditionella recept med omsorg och kärlek samt en touch av nyskapande matlagning.</p>
				<p>Här tillagas klassiska grekiska rätter i både små och stora format som passar utmärkt både för en snabb visit och längre middagar.</p>
				<p>
					Även vår avslappnande uteservering vid Fatbursparken i Stockholm är en del av upplevelsen på <strong><em>Stavros Taverna</em></strong>.
				</p>
			</div>
		</div>
	</div>


</div>

<?php
include 'includes/footer.inc.php';
?>
