<?php
include 'includes/header.inc.php';
include 'includes/nav.inc.php';
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="text-center">
			Find us on <span style="color: #0266C8; padding-left: 10px;">G</span><span style="color: #F90101">o</span><span style="color: #F2B50F">o</span><span style="color: #0266C8">g</span><span style="color: #00933B">l</span><span style="color: #F90101">e</span><span style="padding-left: 10px;">Maps</span>
		</h3>
	</div>
</div>
<div class="row" style="padding: 0px 25px 0px 25px;">
	<div class="Flexible-container">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2036.076591683195!2d18.06948399999999!3d59.31497799999988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x68ba96669aaba599!2sSTAVROS+TAVERNA!5e0!3m2!1sen!2sse!4v1409483039261" width="600" height="450"
			style="border: 0"></iframe>
	</div>
</div>
<!--
<div class="row" style="margin-top: 10px;">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="" style="padding-right: 15px;"><strong><em>Address</em></strong></span> <span><em>Fatburstrappan 20, 118 26 Stockholm</em></span>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="" style="padding-right: 15px;"><strong><em>Phone</em></strong></span> <span><em>076-879 29 59</em></span>
			</div>
		</div>
	</div>
</div>
-->
<div style="margin-top: 10px;"></div>
<?php
include 'includes/footer.inc.php';
?>