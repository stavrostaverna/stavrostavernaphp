<?php
include 'includes/header.inc.php';
include 'includes/nav.inc.php';
?>

<div class="row">
	<div class="col-md-7">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div id="carousel-example-generic" class="carousel slide center">
					<!-- Indicators -->
					<!-- 					<ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					</ol> -->

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<img src="img/stavros-slider-640/s4.jpg"
								alt="stavros taverna grekisk restaurang">
						</div>
						<div class="item">
							<img src="img/stavros-slider-640/s1.jpg"
								alt="Stavros taverna lamb racks">
						</div>
						<div class="item">
							<img src="img/stavros-slider-640/s2.jpg"
								alt="stavros taverna musslor mussels">
						</div>
						<div class="item">
							<img src="img/stavros-slider-640/s3.jpg"
								alt="stavros taverna mojito">
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic"
						data-slide="prev"> <span class="icon-prev"></span>
					</a> <a class="right carousel-control"
						href="#carousel-example-generic" data-slide="next"> <span
						class="icon-next"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel panel-primary">
			<div class="panel-heading info-panel">
				<h3 class="text-center">
					<em><span style="padding-right: 10px;">Grekisk</span><span>Restaurang</span></em>
				</h3>
			</div>
			<div class="panel-body ">
				<p>
					Varmt välkomna till oss på <strong><em>Stavros Taverna</em></strong>,
					en liten Grekisk restaurang på Södermalm i Stockholm.
				</p>
				<p>Vi serverar genuina mezerätter
					&#8220;smårätter&#8221; som lagas enligt traditionella
					recept med omsorg och kärlek samt en touch av nyskapande
					matlagning.</p>
				<p>Här tillagas klassiska grekiska rätter i både
					små och stora format som passar utmärkt både
					för en snabb visit och längre middagar.</p>
				<p>
					Även vår avslappnande uteservering vid Fatbursparken i
					Stockholm är en del av upplevelsen på <strong><em>Stavros
							Taverna</em></strong>.
				</p>
			</div>
		</div>
		<!-- 	<div class="row">
			<div class="col-md-7"> -->
		<div class="panel panel-primary text-center">
			<div class="panel-heading info-panel">
				<h3 class="text-center">
					<em><span style="padding-right: 10px;">Boka</span><span>Bord</span></em>
				</h3>
			</div>
			<div class="panel-body">
				<p>Klicka på bookatable för att boka direkt</p>
				<h3>076-879 29 59 - 58</h3>
				<div>
					<a href="boka.php" class="btn btn-danger"><span class="h3"><i><span>Book</span><span
								style="color: #489DEF;">a</span><span>table</span></i></span></a>
				</div>
			</div>
		</div>
		<!-- 		</div> -->
		<!-- 		<div class="col-md-5">
				<div class="panel panel-primary text-center">
					<div class="panel-heading info-panel">
						<h3 class="text-center">
							<em>Erbjudande</em>
						</h3>
					</div>
					<div class="panel-body" style="padding-bottom: 10px;">
						<p style="font-size: 14px; color: #428bca;">
							<em>Månadens</em> <em>Erbjudande</em>
						</p>
						<p>10 Kalla meze</p>
						<p>samt valfri varmrätt</p>
						<p style="color: #d9534f;">396:-</p>
						<p>För 2 personer</p>
					</div>
				</div>
			</div>
		</div> -->
	</div>
</div>

<div class="panel panel-default">
	<div id="myCarousel" class="carousel slide">

		<!-- Carousel items -->
		<div class="carousel-inner">
			<!--/item-->
			<div class="item active">
				<div class="row">
					<div class="col-sm-3 col-xs-6">
						<a href="#x col-xs-6" class="thumbnail"><img
							src="img/stavros-slider-250/ouzo.jpg" alt="stockholm ouzo"
							class="img-responsive"></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a href="#x" class="thumbnail"><img
							src="img/stavros-slider-250/grekisk.jpg" alt="grekisk restaurang"
							class="img-responsive"></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a href="#x" class="thumbnail"><img
							src="img/stavros-slider-250/table.jpg"
							alt="stavros taverna restaurang" class="img-responsive"></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a href="#x" class="thumbnail"><img
							src="img/stavros-slider-250/ut.jpg"
							alt="stavros taverna fatbursparken" class="img-responsive"></a>
					</div>
				</div>
				<!--/row-->
			</div>
			<!--/item-->
			<div class="item">
				<div class="row">
					<div class="col-sm-3 col-xs-6">
						<a href="#x" class="thumbnail"><img
							src="img/stavros-slider-250/lax.jpg" alt="lax stavros taverna"
							class="img-responsive"></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a href="#x" class="thumbnail"><img
							src="img/stavros-slider-250/souvlaki.jpg"
							alt="souvlaki stockholm" class="img-responsive"></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a href="#x" class="thumbnail"><img
							src="img/stavros-slider-250/tuna.jpg" alt="tuna stavros"
							class="img-responsive"></a>
					</div>
					<div class="col-sm-3 col-xs-6">
						<a href="#x" class="thumbnail"><img
							src="img/stavros-slider-250/salata.jpg" alt="grekisk salad"
							class="img-responsive"></a>
					</div>
				</div>
				<!--/row-->
			</div>
			<!--/item-->
		</div>
		<!--/carousel-inner-->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="icon-prev"></span>
		</a> <a class="right carousel-control" href="#myCarousel"
			data-slide="next"> <span class="icon-prev"></span>
		</a>
	</div>
	<!--/myCarousel-->
</div>


<div class="row">
	<div class="col-md-4">
		<div class="panel panel-primary text-center">
			<div class="panel-heading info-panel">
				<h4>Adress</h4>
			</div>
			<div class="panel-body">
				<address>
					<strong>Stravros Taverna</strong><br> <span
						style="padding-right: 10px;">Fatburstrappan 20,</span> 118 26<br>
					Södermalm, Stockholm, Sweden<br> <abbr title="Phone"></abbr>
					076-879 29 59 <br> <a href="mailto:mail@stavrostaverna.se">mail@stavrostaverna.se</a>
				</address>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-primary">
			<div class="panel-heading text-center info-panel">
				<h4>Hitta oss</h4>
			</div>
			<div class="panel-body">
				<a class="btn btn-block btn-social btn-facebook"
					href="https://www.facebook.com/stavros.taverna.official"> <i
					class="fa fa-facebook"></i>Stavros Taverna on Facebook
				</a> <a class="btn btn-block btn-social btn-instagram"
					href="http://instagram.com/stavrostaverna"> <i
					class="fa fa-instagram"></i>Stavros Taverna on Instagram
				</a>
				<div id="fb-root" style="padding-top: 10px;"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/sv_SE/sdk.js#xfbml=1&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
				</script>
				<div class="fb-like" data-href="" data-layout="button_count"
					data-action="like" data-show-faces="true" data-share="true"></div>

			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-primary text-center">
			<div class="panel-heading info-panel">
				<h4>Öppettider</h4>
			</div>
			<div class="panel-body">
				<p>16:00 - 01:00</p>
			</div>
		</div>
	</div>
</div>

<?php
include 'includes/footer.inc.php';
?>
