<?php
include 'includes/header.inc.php';
include 'includes/nav.inc.php';
?>
<div class="row">
	<div class="col-md-7">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div id="carousel-example-generic" class="carousel slide center">
					<!-- Indicators -->
					<!-- 					<ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					</ol> -->

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<img src="img/over/o3.jpg"
								alt="stavros taverna grekisk restaurang">
						</div>
						<div class="item">
							<img src="img/over/o2.jpg"
								alt="stavros taverna grekisk restaurang">
						</div>
						<div class="item">
							<img src="img/over/o1.jpg"
								alt="stavros taverna grekisk restaurang">
						</div>
						<div class="item">
							<img src="img/over/o4.jpg"
								alt="stavros taverna grekisk restaurang">
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic"
						data-slide="prev"> <span class="icon-prev"></span>
					</a> <a class="right carousel-control"
						href="#carousel-example-generic" data-slide="next"> <span
						class="icon-next"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel panel-primary">
			<div class="panel-heading info-panel">
				<h3 class="text-center">
					<em>Övervåning</em>
				</h3>
			</div>
			<div class="panel-body ">
				<p>Övervåning med glasgolv och utsikt (från 3 stora fönster från
					golv till tak) mot fatbursparken.</p>
				<p>Här kan ni boka för större sällskap upp till 25 personer för
					middag, affärsmöte, vernissage, dop osv.</p>
				<p>Vi skräddarsyr menyer efter era önskemål.</p>
				<p>Vänligen ring för mer information till Lacki på 0768792958.</p>
			</div>
		</div>
	</div>
</div>

<?php
include 'includes/footer.inc.php';
?>
