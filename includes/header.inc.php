<!DOCTYPE html>
<html prefix="og:  http://ogp.me/ns#" lang="sv-SE">

<head>
<title>Grekisk Restaurang Årsta, Farsta & Hägersten | Stavros Taverna</title>

<link rel="shortcut icon" href="img/favicon/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-social.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/stavros.css" rel="stylesheet">
<link href="css/lightbox.css" rel="stylesheet">

<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/lightbox.min.js"></script>


<meta name="author" content="Stavros Taverna">
<meta name="description"
	content="Stavros Taverna är en grekisk restaurang beläget i närheten av Årsta, Farsta & Hägersten i Stockholm. Här serveras grekiska rätter i både små & stora format!" />
<meta name="keywords"
	content="restaurang stockholm,grekisk restaurang,grekisk mat,restaurang soder,restaurang sodermalm,restaurant stockholm,greek restaurant stockholm,restaurang,stockholm,sodermalm,södermalm, grekisk,restaurang,södermalm,stockholm,souvlaki,baklava,fetaoströra, greek, restaurant, stockholm, Restaurang, Mat, Stockholm, grekisk, stockholm, söder,greken, stavros, taverna" />
<meta name="robots" content="index, follow, noarchive" />
<meta name="googlebot" content="noarchive" />

<?php if (stristr($_SERVER["HTTP_USER_AGENT"],'facebook') !== false) { ?>
<meta property="og:title" content="Stavros Taverna" />
<meta property="og:url" content="http://stavrostaverna.se" />
<meta property="og:image" content="http://www.stavrostaverna.se/img/fb/preview.jpg" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="400" />
<meta property="og:locale" content="sv_SE" />
<meta property="og:type" content="website" />
<meta property="og:description"
	content="Grekisk Restaurang på Södermalm, Stockholm. En av de nyaste grekiska restaurangerna i Stockholm, med säte vid medborgarplatsen på söder invid söder torn, vi har även den ända Grekiska menyn i Stockholm som innehåler smårätter som man äter i Grekland." />
<?php } ?>
<?php
if (empty ( $page_css ) === false) {
	foreach ( $page_css as $css_file )
		echo '<link href="css/' . $css_file . '" rel="stylesheet">';
}
if (empty ( $page_js ) === false) {
	foreach ( $page_js as $js_file )
		echo '<script type="text/javascript" src="js/' . $js_file . '"></script>';
}

?>

<script type="text/javascript">
	$(document).ready(function() {
    	$('#carousel-example-generic').carousel({
		interval: 3000
		})
    });
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#myCarousel').carousel({
		interval: 0
		})

	    $('#myCarousel').on('slid.bs.carousel', function() {
	    	//alert("slid");
		});
	});
</script>
<script type='text/javascript'
	src="http://imsky.github.io/holder/holder.js"></script>


</head>

<body>
<?php include_once("analyticstracking.php")?>
	<div class="container">
		<div class="header_cont panel">
			<a href="index.php"><img class="header_img img-responsive"
				src="img/logo.jpg" alt="Stavros Taverna"></a>
		</div>
