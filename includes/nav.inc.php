<nav class="navbar navbar-default navig" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand visible-xs disabled" href="">MENY</a>
		</div>


		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="index.php">Hem</a></li>
				<li><a href="boka.php">Reservation</a></li>
				<li><a href="meny.php">Meny</a></li>
				<li><a href="omoss.php">Om Oss</a></li>
				<li><a href="overvaning.php">&Ouml;verv&aring;ning</a></li>
				<li><a href="karta.php">Karta</a>

			</ul>
		</div>
		<!-- /.navbar-collapse -->
		<!-- /.container-fluid -->
	</div>
</nav>