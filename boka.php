<?php
include 'includes/header.inc.php';
include 'includes/nav.inc.php';
?>


<div class="row">
	<div class="col-md-6">
		<script src="https://secure.livebookings.com/LBDirect/Assets/Scripts/LBDirectDeploy.js"></script>
		<script>
			// <![CDATA[
			LBDirect_Embed({
			  connectionid: "SE-RES-STAVROSTAVERNA_236183:52847",
			  restaurantid: "236183",
			  style:{
			    pageColor: "111bd9",
			    borderStyle: "rounded"
			  },
			  language: "sv-SE",
			  hideLanguageOptions: false
			});
			// ]]>
		</script>
		</div>
		<div class="col-md-6">

			<div class="panel panel-primary text-center">
				<div class="panel-heading info-panel">
					<h4>Book a table</h4>
				</div>
				<div class="panel-body">
					<address>
						<strong>Stravros Taverna</strong><br> <abbr title="Phone"></abbr><b style="padding-right: 10px;">Bokning</b> 076-879 29 59 <br> <abbr title="Phone"></abbr><b style="padding-right: 10px;">Kontor</b> 076-879 29 58 <br> <span style="padding-right: 10px;">Fatburstrappan 20,</span> 118 26<br> S&ouml;dermalm, Stockholm, Sweden<br> <a href="mailto:mail@stavrostaverna.se">mail@stavrostaverna.se</a>
					</address>
				</div>
			</div>

		</div>
	</div>
	<div style="margin-top: 10px;"></div>

<?php
include 'includes/footer.inc.php';
?>
