<?php
$page_css = array (
		"bootstrap-image-gallery.min.css", "blueimp-gallery.min.css"
);
// $page_js = array (
// 		"bootstrap-image-gallery.min.js"
// );

include 'includes/header.inc.php';
include 'includes/nav.inc.php';
?>

<div id="blueimp-gallery" class="blueimp-gallery" data-use-bootstrap-modal="false">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="links">
    <a href="img/stavros-slider-250/ouzo.jpg" title="Banana" data-gallery>
        <img src="img/stavros-slider-250/ouzo.jpg" alt="Banana">
    </a>
    <a href="img/stavros-slider-250/grekisk.jpg" title="Apple" data-gallery>
        <img src="img/stavros-slider-250/grekisk.jpg" alt="Apple">
    </a>
    <a href="img/stavros-slider-250/ut.jpg" title="Orange" data-gallery>
        <img src="img/stavros-slider-250/ut.jpg" alt="Orange">
    </a>
    <a href="img/stavros-slider-250/ut.jpg" title="Orange" data-gallery>
        <img src="img/stavros-slider-250/ut.jpg" alt="Orange">
    </a>
    <a href="img/stavros-slider-250/ut.jpg" title="Orange" data-gallery>
        <img src="img/stavros-slider-250/ut.jpg" alt="Orange">
    </a>
</div>


<div style="margin-top:10px;"></div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/jquery.blueimp-gallery.min.js"></script>
<script src="js/bootstrap-image-gallery.min.js"></script>
<?php
include 'includes/footer.inc.php';
?>